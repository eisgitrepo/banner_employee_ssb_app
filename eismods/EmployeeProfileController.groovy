/*******************************************************************************
 Copyright 2014-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/
package net.hedtech.banner.employee

import grails.converters.JSON
import groovy.util.logging.Slf4j
import net.hedtech.banner.DateUtility
import net.hedtech.banner.payroll.employee.EmployeeUtility
import net.hedtech.banner.general.person.PersonAddressUtility
import net.hedtech.banner.general.person.PersonTelephoneUtility
import net.hedtech.banner.general.person.PersonUtility
import net.hedtech.banner.general.person.view.PersonAddressByRoleView
import net.hedtech.banner.hrcommon.HrControllerBase
import net.hedtech.banner.hrcommon.HrControllerUtility
import net.hedtech.banner.payroll.PayrollMessageUtility
import net.hedtech.banner.payroll.employee.EmployeeBase
import net.hedtech.banner.payroll.employee.EmployeeLeaveBalanceView
import net.hedtech.banner.payroll.employee.LeaveByJobAvailableBalanceView
import net.hedtech.banner.payroll.utils.FormatterUtility
import net.hedtech.banner.payroll.utils.HrSessionUtility
import org.springframework.context.i18n.LocaleContextHolder as LCH
import net.hedtech.banner.general.person.*

@Slf4j
class EmployeeProfileController extends HrControllerBase {

    def payrollUtilityService


    def myEmployeeDetails() {
        Map params = request?.JSON ? HrControllerUtility.unmarshallJSONObject(request.JSON) : params
        def bannerId = HrControllerUtility.parseParam(params.id)
        def pidm = ControllerUtility.getPrincipalPidm()
        Map displayIndicators = new HashMap()

        def model = [:]
        def employeePidm
        def viewSelf = true
        if (bannerId) {
            employeePidm = PersonUtility.getPerson(bannerId)?.pidm
            if (!(EmployeeProfileUtility.hasAccessToEmployee(pidm, employeePidm))) {
                render(status: 403)
                return
            } else {
                viewSelf = false
            }
        } else {
            if (params?.id) {
                employeePidm = PersonUtility.getPerson(params.id)?.pidm
                if (!(EmployeeProfileUtility.hasAccessToEmployee(pidm, employeePidm))) {
                    render(status: 403)
                    return
                } else {
                    viewSelf = false
                }
            } else {
                employeePidm = pidm
            }

        }


        if (!displayIndicators) {
            displayIndicators = EmployeeProfileUtility.getDisplayIndicators()
        }

        model.employee = fetchEmployeeBioDetails(employeePidm, displayIndicators.displayPreferredFirstName, viewSelf)

        model.displayBirthDate = displayIndicators?.displayBirthdate
        model.displayHireDate = displayIndicators?.displayHiredate
        model.displayEmployeeStatus = displayIndicators?.displayEmployeeStatus
        model.displayPhoto = displayIndicators?.displayPhoto
        model.viewSelf = viewSelf

        if (!viewSelf) {
            model.leaveBalances = fetchEmployeeLeaveBalance(employeePidm)
            model.asOfDate = new Date()
        }

        log.debug "Employee detail model = $model"

        render model as JSON
    }


    private fetchEmployeeBioDetails(employeePidm, displayPreferredFirstName, viewSelf) {
        log.debug "Executing fetchEmployeeBioDetails with pidm=$employeePidm"
        def person = PersonUtility.getPerson(employeePidm)
        def basicPerson = PersonBasicPersonBase.fetchByPidm(employeePidm)

        def name = payrollUtilityService.getPreferredName(person.pidm)

        def model = [
                name                            : name,
                bannerId                        : person.bannerId,
                birthDate                       : getBirthDate(basicPerson.birthDate, basicPerson.pidm),
                image                           : g.createLink(controller: 'employeePicture', params: [bannerId: person.bannerId]),
                bannerAddressUpdateURL          : EmployeeProfileUtility.fetchBannerAddressUpdateUrlConfiguration(),
                banner8SSBenefitsEnrollmentURL  : EmployeeProfileUtility.fetchBanner8SSBenefitsEnrollmentUrlConfiguration(),
                bannerAddressUpdateFlag         : EmployeeProfileUtility.fetchBannerAddressUpdate(),
                bannerMorePersonalInfoUpdateURL : EmployeeProfileUtility.fetchBannerMorePersonalInfoUpdateUrlConfiguration(),
                bannerMorePersonalInfoUpdateFlag: EmployeeProfileUtility.fetchBannerMorePersonalInfoUpdate(),
                bannerEmailUpdateURL            : EmployeeProfileUtility.fetchBannerEmailUpdateUrlConfiguration(),
                bannerEmailUpdateFlag           : EmployeeProfileUtility.fetchBannerEmailUpdate(),
                bannerEmergencyContactUpdateURL : EmployeeProfileUtility.fetchBannerEmergencyContactUpdateUrlConfiguration(),
                bannerEmergencyContactUpdateFlag: EmployeeProfileUtility.fetchBannerEmergencyContactUpdate()
        ]


        def employeeBase = EmployeeBase.fetchByPidm(employeePidm)
        def profileHireDateType = HrSessionUtility.getHrSession().profileHireDateType


        if (employeeBase) {
            model.status = g.message(code: PayrollMessageUtility.getEmployeeStatusMessageCode(employeeBase.employeeStatus))
            if (profileHireDateType == "O") {
                model.hired = employeeBase.firstHireDate
            } else if (profileHireDateType == "A") {
                model.hired = employeeBase.adjustedServiceDate
            } else if (profileHireDateType == "S") {
                model.hired = employeeBase.seniorityDate
            } else if (profileHireDateType == "F") {
                model.hired = employeeBase.firstWorkDate
            } else {
                model.hired = employeeBase.currentHireDate
            }

        }

        //    def basicPerson = PersonBasicPersonBase.fetchByPidm(employeePidm)
        model.birthDate = getBirthDate(basicPerson.birthDate, basicPerson.pidm)
        model.confidentialIndicator = (basicPerson.confidIndicator ? basicPerson.confidIndicator.equals("Y") : false)

        //Multiple valued fields
        Map maskingRule = HrSessionUtility.getHrSession().maskingRule
        if (!maskingRule) {
            maskingRule = EmployeeProfileUtility.getMaskingRule("EMPLOYEEPROFILE")
            HrSessionUtility.getHrSession().maskingRule = maskingRule
        }
        def addresses = PersonAddressByRoleView.fetchAddressesByPidmAndRole([pidm: employeePidm, role: 'EMPLOYEE'])

        model.addresses = []
        def personAddress
        addresses.each { it ->
            personAddress = [:]
            personAddress.addressType = it.addressTypeDescription
            personAddress.address = PersonAddressUtility.formatDefaultAddress([houseNumber: it.houseNumber, streetLine1: it.streetLine1, streetLine2: it.streetLine2, streetLine3: it.streetLine3, streetLine4: it.streetLine4,
                                                                               city       : it.city, state: it.state, zip: it.zip, county: it.county, country: it.nation, displayHouseNumber: maskingRule.displayHouseNumber, displayStreetLine4: maskingRule.displayStreetLine4])
            model.addresses << personAddress
        }


        def atyps = addresses.collect { it -> it.addressType }
        if (atyps) {
            def telephones = []
            if (viewSelf) {
                telephones = EmployeeUtility.fetchTelephonesByPidmAndAddressTypes(employeePidm, atyps)
            } else {
                telephones = PersonTelephone.fetchActiveTelephonesByPidmAndAddressTypes([pidm: employeePidm, addressTypes: atyps])
            }
            model.telephones = []
            def telephone
            def personTelephoneMap
            telephones.each { it ->
                telephone = [:]
                telephone.type = it.telephoneType.description + (it.unlistIndicator == 'Y' ? " " + g.message(code: "employee.phone.unlisted") : "")
                personTelephoneMap = [phoneCountry: (maskingRule.displayCountryCode ? it.countryPhone : null), phoneArea: it.phoneArea, phoneNumber: it.phoneNumber, phoneExtension: it.phoneExtension, phoneInternational: (maskingRule.displayInternationalAccess ? it.internationalAccess : null)]
                telephone.phoneNumber = PersonTelephoneUtility.formatPhone(personTelephoneMap)
                telephone.primaryIndicator = it.primaryIndicator == 'Y' ? true : false
                telephone.unlistIndicator = it.unlistIndicator == 'Y' ? true : false
                model.telephones << telephone
            }
            model.telephones.sort { it.type }
        } else model.telephones = []

        /*
           Date: 07/29/2020
           Jira
           TPM and MM
           Replaced baseline email service call with custom email service calls fetchEmployeeEmail and fetchNonEmployeeEmail
        */
        def ueemail = PersonEmail.fetchEmployeeEmail(employeePidm, 'A', 'Y')

        model.emails = []
        ueemail.each { it ->
            model.emails << [emailType: it.emailType.description, emailAddress: it.emailAddress, preferredIndicator: it.preferredIndicator]
        }

        def emails = PersonEmail.fetchNonEmployeeEmail(employeePidm, 'A', 'Y')

        //def emails = PersonEmail.fetchByPidmAndStatusAndWebDisplay(employeePidm, 'A', 'Y')

        //model.emails = []
        emails.each { it ->
            model.emails << [emailType: it.emailType.description, emailAddress: it.emailAddress, preferredIndicator: it.preferredIndicator]
        }

        def emergencyContacts = PersonEmergencyContact.fetchByPidmOrderByPriority(employeePidm)
        def contact
        def contactPhone
        model.emergencyContacts = []
        emergencyContacts.each { it ->
            contact = [:]
            def displayName = payrollUtilityService.getPreferredName(it.lastName, it.firstName, it.middleInitial, it.surnamePrefix)
            contact.name = displayName
            contact.relationship = it.relationship?.description
            contactPhone = [phoneCountry: (maskingRule.displayCountryCode ? it.countryPhone : null), phoneArea: it.phoneArea, phoneNumber: it.phoneNumber, phoneExtension: it.phoneExtension, phoneInternational: null]
            contact.phone = PersonTelephoneUtility.formatPhone(contactPhone)
            contact.address = PersonAddressUtility.formatDefaultAddress([houseNumber: it.houseNumber, streetLine1: it.streetLine1, streetLine2: it.streetLine2, streetLine3: it.streetLine3, streetLine4: it.streetLine4,
                                                                         city       : it.city, state: it.state?.description, zip: it.zip, country: it.nation?.nation, displayStreetLine4: maskingRule.displayStreetLine4, displayHouseNumber: maskingRule.displayHouseNumber])
            model.emergencyContacts << contact
        }
        return model
    }

    private fetchEmployeeLeaveBalance(employeePidm) {
        def accrueLeaveMethod = getLeaveType()
        def model = []
        if (accrueLeaveMethod.leaveType == "E") {
            model = fetchLeaveBalanceByEmployee(employeePidm)
        } else {
            model = fetchLeaveBalanceByJob(employeePidm)
        }
        return model
    }


    private fetchLeaveBalanceByEmployee(employeePidm) {

        def employeeLeaveBalances = EmployeeLeaveBalanceView.fetchByPidm(employeePidm)
        def model = []
        log.debug "Executing fetchEmployeeLeaveBalance with pidm=$employeePidm"
        employeeLeaveBalances.each { it ->
            model << [leaveDescription: it.leaveLongDescription, available: FormatterUtility.formatNumber(it.availableBalance), summaryDescription: ControllerUtility.getConvertedStringType2(it.leaveLongDescription, getHourDayString(it.hourDayInd), "employee.hourday.format.layout.leaveBalance.value.in.hourOrday"), isBalancePositive: ControllerUtility.getBalanceStatus(it.availableBalance)]
        }
        return model
    }


    private fetchLeaveBalanceByJob(employeePidm) {
        def employeeLeaveBalances = LeaveByJobAvailableBalanceView.fetchByPidm(employeePidm)
        def model = []
        log.debug "Executing fetchEmployeeLeaveBalance with pidm=$employeePidm"
        employeeLeaveBalances.each { it ->
            model << [leaveDescription: it.leaveDescription, available: FormatterUtility.formatNumber(it.availableBalance), summaryDescription: ControllerUtility.getConvertedStringType2(it.leaveDescription, getHourDayString(it.hourDayIndicator), "employee.hourday.format.layout.leaveBalance.value.in.hourOrday"), isBalancePositive: it.availableBalance >= 0]
        }
        return model
    }


    private getHourDayInd(hourOrDay) {
        def res

        if (hourOrDay == 'H') res = g.message(code: "employee.hour.format")
        else res = g.message(code: "employee.day.format")

        return res
    }


    private getBirthDate(date, pidm) {
        def res
        def value
        res = g.message(code: "birthdate.minus.year")
        try {
            if (LCH.getLocale().toString().toLowerCase().startsWith("ar")) {
                value = EmployeeUtility.hijriCalenderDate(pidm, res)[0].arabicBirthDate
            } else {
                value = DateUtility.formatDate(date, res)
            }
        }
        catch (IllegalArgumentException x) {
            x.printStackTrace()
            value = date.format('MM-dd')
        }
        return value
    }


    private def getLeaveType() {
        def accrueLeaveMethod = HrSessionUtility.getHrSession().accrueLeaveMethod
        def model = [leaveType: accrueLeaveMethod]
        return model
    }


    private getHourDayString(hourOrDay) {
        def res

        if (hourOrDay == 'H') res = "employee.common.hours"
        else res = "employee.common.days"

        return res
    }


}
