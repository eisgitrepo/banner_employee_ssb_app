/*******************************************************************************
 Copyright 2017 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/

modules = {
    'employeeDashboard' {
        dependsOn "bannerWebLTR, i18n-core, glyphicons, bootstrap, auroraCommon, commonComponents, commonComponentsLTR"
        defaultBundle environment == "development" ? false : "employeeDashboard"

        resource url: [plugin: 'sghe-aurora', file: 'css/aurora-header.css'], attrs: [media: 'screen, projection']
    }

    'employeeDashboardRTL' {
        dependsOn "bannerWebRTL, i18n-core, glyphicons, bootstrap, auroraCommon, commonComponents, commonComponentsRTL"
        defaultBundle environment == "development" ? false : "employeeDashboardRTL"
    }

    'commonComponents' {
        resource url: [file: 'js/d3/d3.min.js']
        resource url: [file: 'js/xe-components/xe-ui-components.js']
    }
    'commonComponentsLTR' {
        resource url: [file: 'css/xe-components/xe-ui-components.min.css']
    }
    'commonComponentsRTL' {
        resource url: [file: 'css/xe-components/xe-ui-components.min-rtl.css']
    }
}