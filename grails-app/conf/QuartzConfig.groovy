/********************************************************************************
 Copyright 2018 Ellucian Company L.P. and its affiliates.
 ********************************************************************************/
quartz {
    autoStartup = true
    waitForJobsToCompleteOnShutdown = true
    exposeSchedulerInRepository = true

    props {
        scheduler.skipUpdateCheck = true
        scheduler.instanceName = 'Banner Quartz Scheduler'
        scheduler.instanceId = 'ESS'

        jobStore.class = 'org.quartz.simpl.RAMJobStore'

        threadPool.threadCount = 1
    }
}
