/*********************************************************************************
 Copyright 2010-2019 Ellucian Company L.P. and its affiliates .
 **********************************************************************************/

// ******************************************************************************
//
//                       +++ EXTERNALIZED CONFIGURATION +++
//
// ******************************************************************************
//
// Config locations should be added to the map used below. They will be loaded based upon this search order:
// 1. Load the configuration file if its location was specified on the command line using -DmyEnvName=myConfigLocation
// 2. Load the configuration file if it exists within the user's .grails directory (i.e., convenient for developers)
// 3. Load the configuration file if its location was specified as a system environment variable
//
// Map [ environment variable or -D command line argument name : file path ]
// in the APPLICATION CONFIGURATION section below.
grails.config.locations = [
        BANNER_APP_CONFIG         : "banner_configuration.groovy",
        BANNER_EMPLOYEE_SSB_CONFIG: "EmployeeSelfService_configuration.groovy"
]

grails.controllers.upload.maxFileSize = -1
grails.controllers.upload.maxRequestSize = -1


// ******************************************************************************
//
//                       +++ DATABASE CONFIGURATION +++
//
// ******************************************************************************
dataSource {
    dialect = "org.hibernate.dialect.Oracle10gDialect"
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = true
    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory'
    show_sql = false
    packagesToScan = "net.hedtech.**.*"
    flush.mode = AUTO
    dialect = "org.hibernate.dialect.Oracle10gDialect"
    config.location = [
            "classpath:hibernate-banner-general-utility.cfg.xml"
    ]
}


// ******************************************************************************
//
//                       +++ ESS CONFIGURATION +++
//
// ******************************************************************************
app.appId="Employee"
app.name="EmployeeSelfService"
app.platform.version="9.33"

server.contextPath='/EmployeeSelfService'


// ******************************************************************************
//
//                       +++ NAME DISPLAY CONFIGURATION +++
//
// ******************************************************************************
// This field are needed for NAME DISPALY API to pick the name pattern from GUANDSP form
productName="EmployeeSelfService"
banner.applicationName="EmployeeSelfService"
banner.ess.pageName="EmployeeProfile"


// ******************************************************************************
//
//                       +++ BUILD NUMBER SEQUENCE UUID +++
//
// ******************************************************************************
//
// A UUID corresponding to this project, which is used by the build number generator.
// Since the build number generator web service provides build number sequences to
// multiple projects, and each project uses a unique UUID to identify which number
// sequence it is using.
//
// This number should NOT be changed.
// FYI: When a new UUID is needed (e.g., for a new project), use this URI:
//      http://m039198.ellucian.com:8080/BuildNumberServer/newUUID
//
// DO NOT EDIT THIS UUID UNLESS YOU ARE AUTHORIZED TO DO SO AND KNOW WHAT YOU ARE DOING
//
build.number.uuid = "6e577a2e-ad5a-4af1-a769-99e77bc5ba9c" // specific UUID for ESS application
build.number.base.url= "http://m037169:8081/BuildNumberServer/buildNumber?method=getNextBuildNumber&uuid="


// ******************************************************************************
//
//                       +++ GRAILS SETTINGS +++
//
// ******************************************************************************
grails.project.groupId = "net.hedtech" // used when deploying to a maven repo
grails.databinding.useSpringBinder=true
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
        html: ['text/html', 'application/xhtml+xml'],
        xml: ['text/xml', 'application/xml', 'application/vnd.sungardhe.student.v0.01+xml'],
        text: 'text/plain',
        js: 'text/javascript',
        rss: 'application/rss+xml',
        atom: 'application/atom+xml',
        css: 'text/css',
        csv: 'text/csv',
        all: '*/*',
        json: ['application/json', 'text/json'],
        form: 'application/x-www-form-urlencoded',
        multipartForm: 'multipart/form-data',
        jpg: 'image/jpeg',
        png: 'image/png',
        gif: 'image/gif',
        bmp: 'image/bmp',
        svg: 'image/svg+xml',
        svgz: 'image/svg+xml',
]

// The default codec used to encode data with ${}
grails.views.default.codec = "html" // none, html, base64  **** note: Setting this to html will ensure html is escaped, to prevent XSS attack ****
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
grails.plugin.springsecurity.logout.afterLogoutUrl = "/"
grails.converters.domain.include.version = true

grails.converters.json.pretty.print = true
grails.converters.json.default.deep = true

// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = false

// enable GSP preprocessing: replace head -> g:captureHead, title -> g:captureTitle, meta -> g:captureMeta, body -> g:captureBody
grails.views.gsp.sitemesh.preprocess = true

grails.resources.mappers.yuicssminify.includes = ['**/*.css']
grails.resources.mappers.yuijsminify.includes = ['**/*.js']
grails.resources.mappers.yuicssminify.excludes = ['**/*.min.css']
grails.resources.mappers.yuijsminify.excludes = ['**/*.min.js']
grails.resources.adhoc.excludes = ['/**/*-custom.css']

// grails.validateable.packages=['net.hedtech.banner.student.registration']

// set per-environment serverURL stem for creating absolute links
environments {
    development{
        grails.resources.debug = true
    }
}


// ******************************************************************************
//
//                       +++ DATA ORIGIN CONFIGURATION +++
//
// ******************************************************************************
// This field is a Banner standard, along with 'lastModifiedBy' and lastModified.
// These properties are populated automatically before an entity is inserted or updated
// within the database. The lastModifiedBy uses the username of the logged in user,
// the lastModified uses the current timestamp, and the dataOrigin uses the value
// specified here:
dataOrigin = "Banner"


// ******************************************************************************
//
//                       +++ FORM-CONTROLLER MAP +++
//
// ******************************************************************************
// This map relates controllers to the Banner forms that it replaces.  This map
// supports 1:1 and 1:M (where a controller supports the functionality of more than
// one Banner form.  This map is critical, as it is used by the security framework to
// set appropriate Banner security role(s) on a database connection. For example, if a
// logged in user navigates to the 'medicalInformation' controller, when a database
// connection is attained and the user has the necessary role, the role is enabled
// for that user and Banner object.

formControllerMap = [
        'hrcommondashboard' : ['SELFSERVICE'],
        'pictures' : ['SELFSERVICE-EMPLOYEE'],
        'selfservicemenu' : ['SELFSERVICE-EMPLOYEE'],
        'employeeprofile' : ['SELFSERVICE-EMPLOYEE'],
        'employeeleavebalance': ['SELFSERVICE-EMPLOYEE'],
        'employeepicture': ['SELFSERVICE-EMPLOYEE'],
        'employerlogo': ['SELFSERVICE-EMPLOYEE'],
        'hrdashboard': ['SELFSERVICE-EMPLOYEE'],
        'jobsummary': ['SELFSERVICE-EMPLOYEE'],
        'jobdetail': ['SELFSERVICE-EMPLOYEE'],
        'paystubdetail': ['SELFSERVICE-EMPLOYEE'],
        'paystubdetailadmin': ['SELFSERVICE-HRADMINRVR'],
        'paystubsummary': ['SELFSERVICE-EMPLOYEE'],
        'paystubsummaryadmin': ['SELFSERVICE-HRADMINRVR'],
        'earningsbydaterange': ['SELFSERVICE-EMPLOYEE'],
        'earningsbyposition': ['SELFSERVICE-EMPLOYEE'],
        'deductionhistory':['SELFSERVICE-EMPLOYEE'],
        'deductiontotals':['SELFSERVICE-EMPLOYEE'],
        'earningsbypositionexcel': ['SELFSERVICE-EMPLOYEE'],
        'myteam'                 : ['SELFSERVICE-EMPLOYEE'],
        'paystubadminsearch'     : ['SELFSERVICE-HRADMINRVR'],
        'survey'                 : ['SELFSERVICE'],
        'useragreement'          : ['SELFSERVICE'],
        'about'                  : ['GUAGMNU'],
        'positiondescription'    : ['SELFSERVICE'],
        'positiondescedit'       : ['SELFSERVICE'],
        'positiondesclist'       : ['SELFSERVICE'],
        'positiondescdata'       : ['SELFSERVICE'],
        'settings'               : ['SELFSERVICE'],
        'admin'                  : ['SELFSERVICE'],
        'approver'               : ['SELFSERVICE'],
        'export'                 : ['SELFSERVICE'],
        'excelexport'            : ['SELFSERVICE'],
        'proxy'                  : ['SELFSERVICE'],
        'imaging'                : ['SELFSERVICE'],
        'positiondescbase'       : ['SELFSERVICE'],
        'effortreporting'        : ['SELFSERVICE'],
        'securityqa'             : ['SELFSERVICE'],
        'laborredistribution'    : ['SELFSERVICE'],
        'personsearch'           : ['SELFSERVICE'],
        'advancesearch'          : ['SELFSERVICE'],
        'financelookup'          : ['SELFSERVICE'],
        'additionalcriteria'     : ['SELFSERVICE'],
        'proxy'                  : ['SELFSERVICE'],
        'effortreportproxy'      : ['SELFSERVICE'],
        'payevent'               : ['SELFSERVICE'],
        'updatedistribution'     : ['SELFSERVICE'],
        'comments'               : ['SELFSERVICE'],
        'laborapprovals'         : ['SELFSERVICE'],
        'laborroutingqueue'      : ['SELFSERVICE'],
        'effortreporting'        : ['SELFSERVICE'],
        'effortreportinglist'    : ['SELFSERVICE'],
        'financelookupeffort'    : ['SELFSERVICE'],
        'periodcode'             : ['SELFSERVICE'],
        'effortcomments'         : ['SELFSERVICE'],
        'effortroutingqueue'     : ['SELFSERVICE'],
        'effortreportaction'     : ['SELFSERVICE'],
        'theme'                  : ['SELFSERVICE'],
        'themeeditor'            : ['SELFSERVICE'],
        'uploadproperties'       : ['SELFSERVICE'],
        'timeentry': ['SELFSERVICE-EMPLOYEE'],
        'timeentrycalendar': ['SELFSERVICE-EMPLOYEE'],
        'timeentrycomment': ['SELFSERVICE-EMPLOYEE'],
        'timeentrydetail': ['SELFSERVICE-EMPLOYEE'],
        'timeentrylabordistribution' : ['SELFSERVICE-EMPLOYEE'],
        'timeentryperiod': ['SELFSERVICE-EMPLOYEE'],
        'timeentryroutingqueue': ['SELFSERVICE-EMPLOYEE'],
        'timeentryleavebalance': ['SELFSERVICE-EMPLOYEE'],
        'timeentryapprovals': ['SELFSERVICE-EMPLOYEE'],
        'timeentryreports': ['SELFSERVICE-EMPLOYEE'],
        'leaverequestapprovals': ['SELFSERVICE-EMPLOYEE'],
        'userpreference'       : ['SELFSERVICE'],
        'shortcut':['SELFSERVICE'],
        'error' : ['SELFSERVICE'],
        'login' : ['SELFSERVICE'],
        'logout' : ['SELFSERVICE']
]


// ******************************************************************************
//
//            +++ SPRING SECURITY, CAS, SAML CONFIGURATION +++
//
// ******************************************************************************
//grails.plugin.springsecurity.useRequestMapDomainClass = false
//grails.plugin.springsecurity.rejectIfNoRule = true
grails.plugin.springsecurity.securityConfigType = grails.plugin.springsecurity.SecurityConfigType.Requestmap
grails.plugin.springsecurity.cas.active = false
grails.plugin.springsecurity.saml.active = false


// ******************************************************************************
//
//                       +++ INTERCEPT-URL MAP +++
//
// ******************************************************************************
grails.plugin.springsecurity.interceptUrlMap = [
        [pattern:'/',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/resetPassword/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/login/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/index**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern: '/assets/**',access: ['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/logout/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/js/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/css/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/images/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/plugins/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/errors/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/error/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/help/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/i18n/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/selfServiceMenu/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/menu**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/logout/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/login/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/keepAlive/data**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/hrCommonDashboard/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/hrApp/components/payStubAdminSearch/payStubAdminSearch.html',access:['ROLE_SELFSERVICE-HRADMINRVR_BAN_DEFAULT_M']],
        [pattern:'/hrApp/components/payStubAdminSearch/payStubAdminSearchResults.html',access:['ROLE_SELFSERVICE-HRADMINRVR_BAN_DEFAULT_M']],
        [pattern:'/hrApp/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/employeePicture/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/employeeProfile/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/employeeLeaveBalance/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/employerLogo/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/hrDashboard/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/jobSummary/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/jobDetail/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/payStubDetail/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/payStubDetailAdmin/**',access:['ROLE_SELFSERVICE-HRADMINRVR_BAN_DEFAULT_M']],
        [pattern:'/ssb/payStubSummary/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/payStubSummaryAdmin/**',access:['ROLE_SELFSERVICE-HRADMINRVR_BAN_DEFAULT_M']],
        [pattern:'/ssb/earningsbydaterange/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/deductionHistory/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/deductionTotals/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/earningsbyposition/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/earningsbypositionexcel/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/myteam/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/paystubadminsearch/**',access:['ROLE_SELFSERVICE-HRADMINRVR_BAN_DEFAULT_M']],
        [pattern:'/ssb/securityQA/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M','ROLE_SELFSERVICE_BAN_DEFAULT_M']],
        [pattern:'/ssb/survey/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M','ROLE_SELFSERVICE_BAN_DEFAULT_M']],
        [pattern:'/ssb/userAgreement/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M','ROLE_SELFSERVICE_BAN_DEFAULT_M']],
        [pattern:'/ssb/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/positionDescription/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/positionDescEdit/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/positionDescList/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/positionDescData/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/admin/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/proxy/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/settings/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/approver/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/imaging/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/excelExport/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/positionDescApp/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/positionDescApp/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/positionDescBase/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/export/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/laborRedistribution/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/advanceSearch/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/financeLookup/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/additionalCriteria/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/payEvent/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/updateDistribution/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/comments/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/laborApprovals/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/laborRoutingQueue/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/accountElement/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/effortReportingApp/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/effortReporting/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/effortReportingList/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/financeLookupEffort/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/periodCode/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/effortComments/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/effortRoutingQueue/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/effortReportAction/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntry/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryCalendar/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryComment/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryDetail/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryPeriod/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryLaborDistribution/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryRoutingQueue/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryApprovals/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/timeEntryReports/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/leaveRequestApprovals/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/timeEntryApp/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/teApp/**',access:['ROLE_SELFSERVICE-EMPLOYEE_BAN_DEFAULT_M']],
        [pattern:'/ssb/userPreference/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/about/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/theme/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/ssb/themeEditor/**',access:['ROLE_SELFSERVICE-WTAILORADMIN_BAN_DEFAULT_M']],
        [pattern:'/ssb/shortcut/**',access:['IS_AUTHENTICATED_ANONYMOUSLY']],
        [pattern:'/**',access:['ROLE_DETERMINED_DYNAMICALLY']]
]


// ******************************************************************************
//
//                       +++ CODENARC RULESETS +++
//
// ******************************************************************************
codenarc.ruleSetFiles="rulesets/banner.groovy"
codenarc.reportName="target/CodeNarcReport.html"
codenarc.propertiesFile="grails-app/conf/codenarc.properties"
codenarc.extraIncludeDirs=["grails-app/composers"]


// ******************************************************************************
//
//                  +++ MISCELLANEOUS CONFIGURATION +++
//
// ******************************************************************************
spring.jmx.enabled=false

defaultResponseHeadersMap = [
        "X-Content-Type-Options": "nosniff",
        "X-XSS-Protection": "1; mode=block"
]

// placeholder for real configuration
// base.dir is probably not defined for .war file deployments
//banner.picturesPath=System.getProperty('base.dir') + '/test/images'

markdown = [
        removeHtml: true
]
