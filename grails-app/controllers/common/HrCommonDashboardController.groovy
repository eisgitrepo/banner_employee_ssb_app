package common

/**
 * Created by inatripath on 3/23/2016.
 */
class HrCommonDashboardController {

    static defaultAction = 'dashboard'
    def dashboard() {
        def map = [:]
        render model: [:], view: "hrCommonDashboard"
    }
}
