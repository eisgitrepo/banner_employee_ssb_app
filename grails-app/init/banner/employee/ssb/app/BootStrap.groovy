/*******************************************************************************
 Copyright 2018-2019 Ellucian Company L.P. and its affiliates.
 *******************************************************************************/
package banner.employee.ssb.app

import grails.converters.JSON
// import grails.util.Environment
import grails.util.Holders
import net.hedtech.banner.converters.json.JSONBeanMarshaller
import net.hedtech.banner.converters.json.JSONDomainMarshaller
import net.hedtech.banner.i18n.JavaScriptMessagesTagLib
import net.hedtech.banner.i18n.LocalizeUtil
import org.apache.commons.logging.LogFactory
// import grails.core.ApplicationAttributes
import org.grails.plugins.web.taglib.ValidationTagLib
import org.grails.web.converters.configuration.ConverterConfiguration
import org.grails.web.converters.configuration.ConvertersConfigurationHolder
import org.grails.web.converters.configuration.DefaultConverterConfiguration
import org.quartz.CronTrigger
import org.quartz.ObjectAlreadyExistsException

import static org.quartz.CronScheduleBuilder.cronSchedule
import static org.quartz.TriggerBuilder.newTrigger

import net.hedtech.banner.payroll.time.TimeEntryNotificationJob

/**
 * Code executed includes:
 * -- Configuring the dataSource to ensure connections are tested prior to use
 * */
class BootStrap {

    def localizer = { mapToLocalize ->
        new ValidationTagLib().message(mapToLocalize)
    }

    def grailsApplication
//    def resourceService
    def dateConverterService
    def javaScriptMessagesTagLib = new JavaScriptMessagesTagLib()

    def init = { servletContext ->

        println "Employee Self-Service Modules..."
        println "Effort Reporting:     ${getEnabledDisabled(Holders?.config?.banner?.SS?.effortReportingAppEnabled)}"
        println "Labor Redistribution: ${getEnabledDisabled(Holders?.config?.banner?.SS?.laborRedistributionAppEnabled)}"
        println "Position Description: ${getEnabledDisabled(Holders?.config?.banner?.SS?.positionDescriptionAppEnabled)}"
        println "Time Entry:           ${getEnabledDisabled(Holders?.config?.banner?.SS?.timeEntryAppEnabled)}"

        // The i18n_core plugin should load this JS file list, however, as of Grails 3.3.2, in August 2019, for whatever
        // reason, sometimes the i18n_core BootStrap fails to run.  So loading it here to ensure that it always happens.
        javaScriptMessagesTagLib.getJsFilesList(grailsApplication)

        grailsApplication.controllerClasses.each {
            log.info "adding log property to controller: $it"
            // Note: weblogic throws an error if we try to inject the method if it is already present
            if (!it.metaClass.methods.find { m -> m.name.matches("getLog") }) {
                def name = it.name // needed as this 'it' is not visible within the below closure...
                try {
                    it.metaClass.getLog = { LogFactory.getLog("$name") }
                }
                catch (e) {
                } // rare case where we'll bury it...
            }
        }

        grailsApplication.allClasses.each {
            if (it.name?.contains("plugin.resource")) {
                log.info "adding log property to plugin.resource: $it"

                // Note: weblogic throws an error if we try to inject the method if it is already present
                if (!it.metaClass.methods.find { m -> m.name.matches("getLog") }) {
                    def name = it.name // needed as this 'it' is not visible within the below closure...
                    try {
                        it.metaClass.getLog = { LogFactory.getLog("$name") }
                    }
                    catch (e) {
                    } // rare case where we'll bury it...
                }
            }
        }

        // Register the JSON Marshallers for format conversion and XSS protection
        registerJSONMarshallers()
//        resourceService.reloadAll()
        try {
            CronTrigger trigger = newTrigger().withIdentity("TimeEntryNotificationJob", "TimeEntryJobSchedulerGroup").withSchedule(cronSchedule(getSchedulerConfiguration()).withMisfireHandlingInstructionFireAndProceed()).build()
            TimeEntryNotificationJob.schedule(trigger)
        } catch (ObjectAlreadyExistsException ex) {
            log.info "Job already exists"
        }
        catch (Exception e) {
            log.error "Error scheduling Time Entry Job"
        }
    }


    def destroy = {
        // no-op
    }


    private def registerJSONMarshallers() {
        Closure marshaller = { it ->
            dateConverterService.parseGregorianToDefaultCalendar(LocalizeUtil.formatDate(it))
        }

        JSON.registerObjectMarshaller(Date, marshaller)

        ConverterConfiguration cfg = ConvertersConfigurationHolder.getNamedConverterConfiguration("deep", JSON.class);
        ((DefaultConverterConfiguration) cfg).registerObjectMarshaller(Date, marshaller);

        def localizeMap = [
                'attendanceHour': LocalizeUtil.formatNumber,
        ]

        JSON.registerObjectMarshaller(new JSONBeanMarshaller(localizeMap), 3) // for decorators and maps
        JSON.registerObjectMarshaller(new JSONDomainMarshaller(localizeMap, true), 4) // for domain objects
    }

    private String getSchedulerConfiguration() {
        def cronScheduleConfiguration = Holders.config.ess.timeEntry?.scheduler?.cronJobConfig
        if (cronScheduleConfiguration != null && cronScheduleConfiguration instanceof String) {
            return cronScheduleConfiguration
        }
        return "0 0 0 * * ?"
    }


    private String getEnabledDisabled(String configValue) {
        return (configValue in ['y','Y'] ? 'Enabled' : 'DISABLED')
    }

}