<%-- Copyright 2013 Ellucian Company L.P. and its affiliates. --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, height=device-height,  initial-scale=1.0, user-scalable=no, user-scalable=0"/>
    <g:set var="mep" value="${params?.mepCode}"/>
    <g:set var="hideSSBHeaderComps" value="${params?.hideSSBHeaderComps}" />
    <g:if test="${mep && hideSSBHeaderComps}">
        <g:set var="url" value="${'ssb/hrDashboard?mepCode='+mep+'&hideSSBHeaderComps='+hideSSBHeaderComps}" />
    </g:if>
    <g:elseif test="${mep}">
        <g:set var="url" value="${'ssb/hrDashboard?mepCode='+mep}" />
    </g:elseif>
    <g:elseif test="${hideSSBHeaderComps}">
        <g:set var="url" value="${'ssb/hrDashboard?hideSSBHeaderComps='+hideSSBHeaderComps}" />
    </g:elseif>
    <g:else>
        <g:set var="url" value="${'ssb/hrDashboard'}" />
    </g:else>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta HTTP-EQUIV="REFRESH" content="0; url=${url}">
</head>
<body>
</body>
</html>
