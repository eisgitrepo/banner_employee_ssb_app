<%--
/*******************************************************************************
Copyright 2014 Ellucian Company L.P. and its affiliates.
*******************************************************************************/
--%>
<html xmlns:ng="http://angularjs.org" ng-app="cmApp" id="ng-app">

<head>
    <g:applyLayout name="bannerWebPage">
        <title><g:message code="communication.landingPage.pageTitle"/></title>

        <meta name="menuEndPoint" content="${request.contextPath}/ssb/menu"/>
        <meta name="menuBaseURL" content="${request.contextPath}/ssb"/>
        <meta name="menuDefaultBreadcrumbId" content=""/>
        %{--<meta name="searchEndpoint" content="${g.createLink(controller: 'selfServiceMenu', action: 'data')}"/>--}%

        <script type="text/javascript">
            document.getElementsByName('menuDefaultBreadcrumbId')[0].content = [
                "<g:message code="communication.breadcrumb.bannerSelfService"/>",
                "<g:message code="communication.breadcrumb.communicationLandingPage"/>"
            ].join('_');
        </script>

        <r:require modules="communication,bootstrap,glyphicons"/>
    </g:applyLayout>
    <meta name="viewport" content="width=device-width, initial-scale=1.000" />
    <ckeditor:resources/>

    <g:bannerMessages/>
    <!--[if lte IE 8]>
      <script>
        document.createElement('ng-include');
        document.createElement('ng-pluralize');
        document.createElement('ng-view');

        // Optionally these for CSS
        document.createElement('ng:include');
        document.createElement('ng:pluralize');
        document.createElement('ng:view');
      </script>
    <![endif]-->

</head>

<body>
    <div id="content">
        <div ui-view ></div>
    </div>
</body>
</html>
