<?xml version="1.0" encoding="UTF-8" ?>
<!-- Copyright -->
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                exclude-result-prefixes="exsl">

    <!-- Overall page layout. -->
    <xsl:attribute-set name="page">
        <xsl:attribute name="page-height">11in</xsl:attribute>
        <xsl:attribute name="page-width">8.5in</xsl:attribute>
        <xsl:attribute name="margin">.25in, .25in, .25in, .25in</xsl:attribute>
    </xsl:attribute-set>

    <!-- Header region for the employer logo and address when printed at top of the page. -->
    <xsl:attribute-set name="header-region">
        <xsl:attribute name="extent">.5in</xsl:attribute>
    </xsl:attribute-set>

    <!-- Footer region for the employer logo and address when printed at bottom of the page. -->
    <xsl:attribute-set name="footer-region">
        <xsl:attribute name="extent">.5in</xsl:attribute>
    </xsl:attribute-set>

    <!-- Body region for main content of the page. -->
    <xsl:attribute-set name="body-region">
        <xsl:attribute name="margin-top">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoTopBottom = 'TOP'">
                    <xsl:text>.5in</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>0in</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="margin-bottom">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/logoTopBottom = 'BOTTOM'">
                    <xsl:text>.7in</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>0in</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>

    <!-- Container content within a region - font styles, writing direction -->
    <xsl:attribute-set name="container">
        <xsl:attribute name="font-family">
            <xsl:value-of select="$locale-font-family"/>
        </xsl:attribute>
        <xsl:attribute name="font-size">6pt</xsl:attribute>
        <xsl:attribute name="font-weight">normal</xsl:attribute>
        <xsl:attribute name="writing-mode">
            <xsl:value-of select="$base-writing-mode"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">
            <xsl:value-of select="$text-align-left"/>
        </xsl:attribute>
    </xsl:attribute-set>

    <!-- Section content within a container -->
    <xsl:attribute-set name="section">
        <xsl:attribute name="margin-top">4mm</xsl:attribute>
        <xsl:attribute name="keep-together.within-column">always</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table styles. -->
    <xsl:attribute-set name="table">
        <xsl:attribute name="table-layout">fixed</xsl:attribute>
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="border">.75pt solid rgb(221, 221, 221)</xsl:attribute>
        <xsl:attribute name="border-collapse">collapse</xsl:attribute>
        <xsl:attribute name="background-color">transparent</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table caption styles. -->
    <xsl:attribute-set name="table-caption">
        <xsl:attribute name="font-size">8pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="margin-bottom">2pt</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table cell styles  -->
    <xsl:attribute-set name="table-cell">
        <xsl:attribute name="background-color">transparent</xsl:attribute>
        <xsl:attribute name="margin">0pt, 0pt, 0pt, 0pt</xsl:attribute>
        <xsl:attribute name="padding">3pt, 2pt, 2pt, 2pt</xsl:attribute>
        <xsl:attribute name="text-align">
            <xsl:value-of select="$text-align-left"/>
        </xsl:attribute>
    </xsl:attribute-set>

    <!-- Table column header cell styling -->
    <xsl:attribute-set name="table-column-header"  use-attribute-sets="table-cell">
        <xsl:attribute name="border">.75pt solid rgb(221, 221, 221)</xsl:attribute>
        <xsl:attribute name="background-color">rgb(242, 242, 242)</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table row header cell styling -->
    <xsl:attribute-set name="table-row-header"  use-attribute-sets="table-cell">
        <xsl:attribute name="border">.75pt solid rgb(221, 221, 221)</xsl:attribute>
        <xsl:attribute name="background-color">rgb(242, 242, 242)</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <!-- Table data cell style -->
    <xsl:attribute-set name="table-data" use-attribute-sets="table-cell">
        <xsl:attribute name="border-top">.75pt solid rgb(221, 221, 221)</xsl:attribute>
        <xsl:attribute name="border-bottom">.75pt solid rgb(221, 221, 221)</xsl:attribute>
        <!-- Workaround for table layout issue with FOP 1.1 -->
        <xsl:attribute name="border-left">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/languageDirection = 'rtl'">
                    <xsl:text>.75pt solid rgb(221, 221, 221)</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>none</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="border-right">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/languageDirection = 'rtl'">
                    <xsl:text>.75pt solid rgb(221, 221, 221)</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>none</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
        <!-- end workaround -->
    </xsl:attribute-set>

    <!-- Table data cell with row header style -->
    <xsl:attribute-set name="table-data-with-row-header" use-attribute-sets="table-cell">
        <xsl:attribute name="border">.75pt solid rgb(221, 221, 221)</xsl:attribute>
    </xsl:attribute-set>

    <!-- Modifies the table cell style for numeric fields the need to decimal align. -->
    <xsl:attribute-set name="table-cell.fixed-length">
        <xsl:attribute name="text-align">center</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="table-cell.fixed-decimal">
        <xsl:attribute name="text-align">right</xsl:attribute>
    </xsl:attribute-set>

    <!-- Modifies the table cell style for total cells. -->
    <xsl:attribute-set name="table-cell.total">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <xsl:attribute-set name="no-data-msg">
        <xsl:attribute name="margin">0pt</xsl:attribute>
        <xsl:attribute name="padding">3pt</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="width">100%</xsl:attribute>
        <xsl:attribute name="border">1pt solid rgb(221, 221, 221)</xsl:attribute>
    </xsl:attribute-set>

    <!--
    =====================================================================================================
         The following attribute definitions are for section specific elements.
         Many of the attributes inherit common styles using use-attribute-sets which produces a similar
         effect to cascading stylesheets.
    =====================================================================================================
     -->
    <!-- Employer section styles -->
    <xsl:attribute-set name="employer-table" use-attribute-sets="table">
        <xsl:attribute name="border">none</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="employer-logo-column">
        <xsl:attribute name="column-width">17%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="employer-contact-info-column">
        <xsl:attribute name="column-width">12%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="employer-name">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="employer-data" use-attribute-sets="table-cell">
        <xsl:attribute name="font-size">80%</xsl:attribute>
        <xsl:attribute name="padding">2pt, 1pt, 1pt, 1pt</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="employer-logo">
        <xsl:attribute name="src">
            <xsl:value-of select="concat('url(', /root/employer/logoPath, '/', /root/employer/logoFilename, ')')"/>
        </xsl:attribute>
        <xsl:attribute name="text-align">left</xsl:attribute>
        <xsl:attribute name="content-width">scale-to-fit</xsl:attribute>
        <xsl:attribute name="content-height">auto</xsl:attribute>
        <xsl:attribute name="width">auto</xsl:attribute>
        <xsl:attribute name="scaling">uniform</xsl:attribute>
    </xsl:attribute-set>

    <!-- Pay Summary/Basic Info combo section styles -->
    <xsl:attribute-set name="summary-pay-summary-column">
        <xsl:attribute name="column-width">50%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="summary-basic-info-column">
        <xsl:attribute name="column-width">50%</xsl:attribute>
    </xsl:attribute-set>

    <!-- Pay Summary section styles -->
    <xsl:attribute-set name="pay-summary-block">
        <xsl:attribute name="margin-right">8pt</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-summary-type-column">
        <xsl:attribute name="column-width">50%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-summary-current-column">
        <xsl:attribute name="column-width">25%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-summary-ytd-column">
        <xsl:attribute name="column-width">25%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-summary-pay-date-header" use-attribute-sets="table-column-header">
        <xsl:attribute name="background-color">rgb(228, 244, 248)</xsl:attribute>
        <xsl:attribute name="font-size">130%</xsl:attribute>
        <xsl:attribute name="text-align">center</xsl:attribute>
        <xsl:attribute name="number-columns-spanned">
            <xsl:choose>
                <xsl:when test="exsl:node-set($config)/displayYTDAmount = 'true'">
                    <xsl:text>3</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>2</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-summary-net" use-attribute-sets="table-data-with-row-header">
        <xsl:attribute name="background-color">rgb(242, 242, 242)</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>

    <!-- Basic Info section styles -->
    <xsl:attribute-set name="basic-info-table" use-attribute-sets="table">
        <xsl:attribute name="border">none</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="basic-info-label-column">
        <xsl:attribute name="column-width">30%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="basic-info-data-column">
        <xsl:attribute name="column-width">70%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="basic-info-data" use-attribute-sets="table-cell">
        <xsl:attribute name="padding">2pt, 1pt, 1pt, 1pt</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="basic-info-data.label">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
     </xsl:attribute-set>

    <!-- Earnings section styles -->
    <xsl:attribute-set name="earnings-position-suffix-column">
        <xsl:attribute name="column-width">10%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-job-title-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-description-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-shift-column">
        <xsl:attribute name="column-width">5%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-hours-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-rate-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-amount-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-amount-ytd-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-non-cash-title" use-attribute-sets="table-data">
        <xsl:attribute name="number-columns-spanned">2</xsl:attribute>
        <xsl:attribute name="font-size">110%</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="earnings-total-label" use-attribute-sets="table-data">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-align">right</xsl:attribute>
    </xsl:attribute-set>

    <!-- Deduction section styles -->
    <xsl:attribute-set name="deductions-description-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-employee-amount-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-employee-amount-ytd-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-employer-amount-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-employer-amount-ytd-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-applicable-gross-column">
        <xsl:attribute name="column-width">13%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-applicable-gross-ytd-column">
        <xsl:attribute name="column-width">15%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-section-title" use-attribute-sets="table-data">
        <xsl:attribute name="number-columns-spanned">2</xsl:attribute>
        <xsl:attribute name="font-size">110%</xsl:attribute>
        <xsl:attribute name="font-weight">bold</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="deductions-total-label" use-attribute-sets="table-data">
        <xsl:attribute name="font-weight">bold</xsl:attribute>
        <xsl:attribute name="text-align">right</xsl:attribute>
    </xsl:attribute-set>

    <!-- Taxable benefits section styles -->
    <xsl:attribute-set name="taxable-benefits-description-column">
        <xsl:attribute name="column-width">50%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="taxable-benefits-amount-column">
        <xsl:attribute name="column-width">25%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="taxable-benefits-amount-ytd-column">
        <xsl:attribute name="column-width">25%</xsl:attribute>
    </xsl:attribute-set>

    <!-- Pay Documents section styles -->
    <xsl:attribute-set name="pay-documents-number-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-documents-type-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-documents-bank-name-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-documents-account-type-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>
    <xsl:attribute-set name="pay-documents-amount-column">
        <xsl:attribute name="column-width">20%</xsl:attribute>
    </xsl:attribute-set>

    <!-- Pay Messages section styles -->
    <xsl:attribute-set name="pay-messages-message-column">
        <xsl:attribute name="column-width">100%</xsl:attribute>
    </xsl:attribute-set>

    <!-- Filing Statuses section styles -->

</xsl:stylesheet>