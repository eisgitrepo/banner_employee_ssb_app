<?xml version="1.0" encoding="UTF-16"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:exsl="http://exslt.org/common"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="exsl fo">

    <xsl:include href="../common/common-config.xsl"/>
    <xsl:include href="payStub-styles.xsl"/>
    <xsl:include href="payStub-styles-custom.xsl"/>

    <xsl:template match="root">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="pay-stub-page"
                                       xsl:use-attribute-sets="page">
                    <fo:region-body xsl:use-attribute-sets="body-region"/>
                    <fo:region-before xsl:use-attribute-sets="header-region"/>
                    <fo:region-after xsl:use-attribute-sets="footer-region"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="pay-stub-page">

                <xsl:if test="exsl:node-set($config)/logoTopBottom = 'TOP'">
                    <fo:static-content flow-name="xsl-region-before">
                        <fo:block-container xsl:use-attribute-sets="container">
                            <xsl:apply-templates select="employer"/>
                        </fo:block-container>
                    </fo:static-content>
                </xsl:if>

                <xsl:if test="exsl:node-set($config)/logoTopBottom = 'BOTTOM'">
                    <fo:static-content flow-name="xsl-region-after">
                        <fo:block-container xsl:use-attribute-sets="container">
                            <xsl:apply-templates select="employer"/>
                        </fo:block-container>
                    </fo:static-content>
                </xsl:if>

                <fo:flow flow-name="xsl-region-body">
                    <fo:block-container xsl:use-attribute-sets="container">
                        <fo:block xsl:use-attribute-sets="section">
                            <!-- Summary section. Includes the pay summary table and basic info. -->
                            <fo:table>
                                <fo:table-column xsl:use-attribute-sets="summary-pay-summary-column"/>
                                <fo:table-column xsl:use-attribute-sets="summary-basic-info-column"/>
                                <fo:table-body>
                                    <fo:table-row>
                                        <fo:table-cell>
                                            <xsl:apply-templates select="paySummary"/>
                                        </fo:table-cell>
                                        <fo:table-cell>
                                            <xsl:apply-templates select="basicInfo"/>
                                        </fo:table-cell>
                                    </fo:table-row>
                                </fo:table-body>
                            </fo:table>
                        </fo:block>

                        <xsl:apply-templates select="earnings"/>
                        <xsl:apply-templates select="deductions"/>
                        <xsl:apply-templates select="federallyTaxableBenefits"/>
                        <xsl:apply-templates select="payDocuments"/>
                        <xsl:apply-templates select="payMessages"/>
                        <xsl:apply-templates select="filingStatuses"/>
                    </fo:block-container>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>

    <xsl:template match="*">
        <xsl:choose>
            <xsl:when test="string(.) = 'null'">
                <xsl:text> </xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="employer">
        <fo:block>
            <fo:table xsl:use-attribute-sets="employer-table">
                <xsl:choose>
                    <xsl:when test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                 and $base-writing-mode = 'lr')
                                                 or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                 and $base-writing-mode = 'rl')">
                        <fo:table-column xsl:use-attribute-sets="employer-logo-column"/>
                        <fo:table-column xsl:use-attribute-sets="employer-contact-info-column"/>
                        <fo:table-column/>
                    </xsl:when>
                    <xsl:otherwise>
                        <fo:table-column/>
                        <fo:table-column xsl:use-attribute-sets="employer-contact-info-column"/>
                        <fo:table-column xsl:use-attribute-sets="employer-logo-column"/>
                    </xsl:otherwise>
                </xsl:choose>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                 and $base-writing-mode = 'lr')
                                                 or (exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                 and $base-writing-mode = 'rl')">
                                    <fo:external-graphic xsl:use-attribute-sets="employer-logo"/>
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="employer-data">
                            <fo:block>
                                <xsl:apply-templates select="name"/>
                            </fo:block>
                            <xsl:apply-templates select="address/addressLine1"/>
                            <xsl:apply-templates select="address/addressLine2"/>
                            <xsl:apply-templates select="address/addressLine3"/>
                            <xsl:apply-templates select="address/addressLine4"/>
                            <xsl:apply-templates select="address/addressLine5"/>
                            <xsl:apply-templates select="address/addressLine6"/>
                            <xsl:apply-templates select="address/addressLine7"/>
                            <xsl:apply-templates select="address/addressLine8"/>
                            <fo:block>
                                <xsl:apply-templates select="phoneNumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell>
                            <fo:block>
                                <xsl:if test="(exsl:node-set($config)/logoLeftRight = 'RIGHT'
                                                 and $base-writing-mode = 'lr')
                                                 or (exsl:node-set($config)/logoLeftRight = 'LEFT'
                                                 and $base-writing-mode = 'rl')">
                                    <fo:external-graphic xsl:use-attribute-sets="employer-logo"/>
                                </xsl:if>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="address/node()">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="paySummary">
        <fo:block xsl:use-attribute-sets="pay-summary-block">
            <fo:table xsl:use-attribute-sets="table">
                <fo:table-column xsl:use-attribute-sets="pay-summary-type-column"/>
                <fo:table-column xsl:use-attribute-sets="pay-summary-current-column"/>
                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                    <fo:table-column xsl:use-attribute-sets="pay-summary-ytd-column"/>
                </xsl:if>
                <fo:table-header>
                    <xsl:if test="string(payDate)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="pay-summary-pay-date-header">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/summaryPayDate"/>&#160;<xsl:apply-templates
                                        select="payDate"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/summaryType"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/summaryCurrent"/>
                            </fo:block>
                        </fo:table-cell>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/summaryYTD"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:if>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="table-row-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/summaryGross"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-data-with-row-header table-cell.fixed-decimal">
                            <fo:block>
                                <xsl:apply-templates select="currentGrossAmount"/>
                            </fo:block>
                        </fo:table-cell>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-cell xsl:use-attribute-sets="table-data-with-row-header table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="ytdGrossAmount"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:if>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="table-row-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/summaryDeductions"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-data-with-row-header table-cell.fixed-decimal">
                            <fo:block>
                                <xsl:apply-templates select="currentDeductionAmount"/>
                            </fo:block>
                        </fo:table-cell>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-cell xsl:use-attribute-sets="table-data-with-row-header table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="ytdDeductionAmount"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:if>
                    </fo:table-row>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="table-row-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/summaryNet"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="pay-summary-net table-cell.fixed-decimal">
                            <fo:block>
                                <xsl:apply-templates select="currentNetAmount"/>
                            </fo:block>
                        </fo:table-cell>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-cell xsl:use-attribute-sets="pay-summary-net table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="ytdNetAmount"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:if>
                    </fo:table-row>
                    <xsl:if test="exsl:node-set($config)/dispSummaryEmployerContributions = 'true'">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="table-row-header">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/summaryEmployerContributions"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="table-data-with-row-header table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="currentEmployerContributions"/>
                                </fo:block>
                            </fo:table-cell>
                            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                <fo:table-cell xsl:use-attribute-sets="table-data-with-row-header table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="ytdEmployerContributions"/>
                                    </fo:block>
                                </fo:table-cell>
                            </xsl:if>
                        </fo:table-row>
                    </xsl:if>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="basicInfo">
        <fo:block>
            <fo:table xsl:use-attribute-sets="basic-info-table">
                <fo:table-column xsl:use-attribute-sets="basic-info-label-column"/>
                <fo:table-column xsl:use-attribute-sets="basic-info-data-column"/>
                <fo:table-body>
                    <xsl:if test="string(bannerId)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data basic-info-data.label">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/basicInfoId"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data">
                                <fo:block>
                                    <xsl:apply-templates select="bannerId"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                    <xsl:if test="string(ssn)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data basic-info-data.label">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/basicInfoSSN"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data">
                                <fo:block>
                                    <xsl:apply-templates select="ssn"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                    <xsl:if test="string(name)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data basic-info-data.label">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/basicInfoName"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data">
                                <fo:block>
                                    <xsl:apply-templates select="name"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                    <xsl:if test="string(primaryAddress/addressLine1)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data basic-info-data.label">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/basicInfoAddress"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data">
                                <xsl:apply-templates select="primaryAddress/addressLine1"/>
                                <xsl:apply-templates select="primaryAddress/addressLine2"/>
                                <xsl:apply-templates select="primaryAddress/addressLine3"/>
                                <xsl:apply-templates select="primaryAddress/addressLine4"/>
                                <xsl:apply-templates select="primaryAddress/addressLine5"/>
                                <xsl:apply-templates select="primaryAddress/addressLine6"/>
                                <xsl:apply-templates select="primaryAddress/addressLine7"/>
                                <xsl:apply-templates select="primaryAddress/addressLine8"/>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                    <xsl:if test="string(payPeriodDates)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data basic-info-data.label">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/basicInfoPayPeriod"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data">
                                <fo:block>
                                    <xsl:apply-templates select="payPeriodDates"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                    <xsl:if test="string(displayPayNumber)">
                        <fo:table-row>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data basic-info-data.label">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/basicInfoPayNumber"/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell xsl:use-attribute-sets="basic-info-data">
                                <fo:block>
                                    <xsl:apply-templates select="displayPayNumber"/>
                                </fo:block>
                            </fo:table-cell>
                        </fo:table-row>
                    </xsl:if>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="primaryAddress/node()">
        <fo:block>
            <xsl:apply-templates/>
        </fo:block>
    </xsl:template>

    <xsl:template match="federallyTaxableBenefits">
        <fo:block xsl:use-attribute-sets="section">
            <fo:block xsl:use-attribute-sets="table-caption">
                <xsl:apply-templates select="exsl:node-set($labels)/taxableBenefitsTitle"/>
            </fo:block>
            <fo:table xsl:use-attribute-sets="table">
                <fo:table-column xsl:use-attribute-sets="taxable-benefits-description-column"/>
                <fo:table-column xsl:use-attribute-sets="taxable-benefits-amount-column"/>
                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                    <fo:table-column xsl:use-attribute-sets="taxable-benefits-amount-ytd-column"/>
                </xsl:if>

                <fo:table-header>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/taxableBenefitsBenefit"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/taxableBenefitsAmount"/>
                            </fo:block>
                        </fo:table-cell>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="exsl:node-set($labels)/taxableBenefitsYTDAmount"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:if>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <xsl:apply-templates select="federallyTaxableBenefit"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="federallyTaxableBenefit">
        <fo:table-row>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="description"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="amount"/>
                </fo:block>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="ytdAmount"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="payDocuments">
        <fo:block xsl:use-attribute-sets="section">
            <fo:block xsl:use-attribute-sets="table-caption">
                <xsl:apply-templates select="exsl:node-set($labels)/payDocumentTitle"/>
            </fo:block>
            <fo:table xsl:use-attribute-sets="table">
                <fo:table-column xsl:use-attribute-sets="pay-documents-number-column"/>
                <fo:table-column xsl:use-attribute-sets="pay-documents-type-column"/>
                <fo:table-column xsl:use-attribute-sets="pay-documents-bank-name-column"/>
                <fo:table-column xsl:use-attribute-sets="pay-documents-account-type-column"/>
                <fo:table-column xsl:use-attribute-sets="pay-documents-amount-column"/>

                <fo:table-header>
                    <fo:table-row>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/payDocumentNumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/payDocumentDocumentType"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/payDocumentBankName"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/payDocumentAccountType"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                            <fo:block>
                                <xsl:apply-templates select="exsl:node-set($labels)/payDocumentAmount"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <xsl:apply-templates select="payDocument"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="payDocument">
        <fo:table-row>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="number"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="documentType"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="bankName"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="accountType"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="amount"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="payMessages">
        <fo:block xsl:use-attribute-sets="section">
            <fo:block xsl:use-attribute-sets="table-caption">Messages</fo:block>
            <fo:table xsl:use-attribute-sets="table">
                <fo:table-column xsl:use-attribute-sets="pay-messages-message-column"/>
                <fo:table-body>
                    <xsl:apply-templates select="payMessage" mode="parent"/>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="payMessage" mode="parent">
        <fo:table-row>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="."/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>


    <xsl:template match="filingStatuses">
        <xsl:apply-templates select="filingStatus"/>
    </xsl:template>

    <xsl:template match="filingStatus">
        <fo:block xsl:use-attribute-sets="section">
            <fo:block xsl:use-attribute-sets="table-caption">
                <xsl:apply-templates select="taxDescription"/>
            </fo:block>
            <fo:table xsl:use-attribute-sets="table">
                <fo:table-header>
                    <fo:table-row>
                        <xsl:for-each select="./options">
                            <fo:table-cell xsl:use-attribute-sets="table-column-header">
                                <fo:block>
                                    <xsl:apply-templates select="title"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:for-each>
                        <xsl:for-each select="./amounts">
                            <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="title"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:for-each>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <fo:table-row>
                        <xsl:for-each select="./options">
                            <fo:table-cell xsl:use-attribute-sets="table-data">
                                <fo:block>
                                    <xsl:apply-templates select="value"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:for-each>
                        <xsl:for-each select="./amounts">
                            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                                <fo:block>
                                    <xsl:apply-templates select="value"/>
                                </fo:block>
                            </fo:table-cell>
                        </xsl:for-each>
                    </fo:table-row>
                </fo:table-body>
            </fo:table>
        </fo:block>
    </xsl:template>

    <xsl:template match="earnings">
        <fo:block xsl:use-attribute-sets="section">
            <fo:block xsl:use-attribute-sets="table-caption">
                <xsl:apply-templates select="exsl:node-set($labels)/earningsTitle"/>
            </fo:block>
            <xsl:choose>
                <xsl:when test="noEarningsData = 'true'">
                    <fo:block xsl:use-attribute-sets="no-data-msg">
                        <xsl:apply-templates select="exsl:node-set($labels)/noEarningsData"/>
                    </fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:table xsl:use-attribute-sets="table">
                        <xsl:if test="exsl:node-set($config)/displayEarningsJobPositionSuffix = 'true'">
                            <fo:table-column xsl:use-attribute-sets="earnings-position-suffix-column"/>
                        </xsl:if>
                        <xsl:if test="exsl:node-set($config)/displayEarningsJobTitle = 'true'">
                            <fo:table-column xsl:use-attribute-sets="earnings-job-title-column"/>
                        </xsl:if>
                        <fo:table-column xsl:use-attribute-sets="earnings-description-column"/>
                        <fo:table-column xsl:use-attribute-sets="earnings-shift-column"/>
                        <fo:table-column xsl:use-attribute-sets="earnings-hours-column"/>
                        <fo:table-column xsl:use-attribute-sets="earnings-rate-column"/>
                        <fo:table-column xsl:use-attribute-sets="earnings-amount-column"/>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-column xsl:use-attribute-sets="earnings-amount-ytd-column"/>
                        </xsl:if>

                        <!-- Print earning headers -->
                        <fo:table-header>
                            <fo:table-row>
                                <xsl:if test="exsl:node-set($config)/displayEarningsJobPositionSuffix = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-column-header">
                                        <fo:block>
                                            <xsl:apply-templates select="exsl:node-set($labels)/earningsJob"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                                <xsl:if test="exsl:node-set($config)/displayEarningsJobTitle = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-column-header">
                                        <fo:block>
                                            <xsl:apply-templates select="exsl:node-set($labels)/earningsJobTitle"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/earningsDescription"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-length">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/earningsShift"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/earningsHours"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/earningsRate"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/earningsAmount"/>
                                    </fo:block>
                                </fo:table-cell>

                                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                        <fo:block>
                                            <xsl:apply-templates select="exsl:node-set($labels)/earningsYTDAmount"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                            </fo:table-row>
                        </fo:table-header>

                        <fo:table-body>
                            <xsl:apply-templates select="cashEarnings"/>
                            <xsl:apply-templates select="nonCashEarnings"/>
                        </fo:table-body>

                    </fo:table>
                </xsl:otherwise>
            </xsl:choose>
        </fo:block>
    </xsl:template>

    <xsl:template match="cashEarnings">
        <xsl:apply-templates select="details" mode="earnings"/>

        <!-- Print earning totals -->
        <fo:table-row>
            <xsl:if test="exsl:node-set($config)/displayEarningsJobPositionSuffix = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block/>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayEarningsJobTitle = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block/>
                </fo:table-cell>
            </xsl:if>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="earnings-total-label">
                <fo:block>
                    <xsl:apply-templates select="exsl:node-set($labels)/earningsTotal"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.total table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="totalAmount"/>
                </fo:block>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.total table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="totalYTDAmount"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="nonCashEarnings">
        <fo:table-row>
            <fo:table-cell xsl:use-attribute-sets="earnings-non-cash-title">
                <fo:block>
                    <xsl:apply-templates select="exsl:node-set($labels)/earningsNonCashTitle"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayEarningsJobPositionSuffix = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayEarningsJobTitle = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
        </fo:table-row>

        <xsl:apply-templates select="details" mode="earnings"/>
    </xsl:template>

    <xsl:template match="details" mode="earnings">
        <fo:table-row>
            <xsl:if test="exsl:node-set($config)/displayEarningsJobPositionSuffix = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                        <xsl:apply-templates select="positionSuffix"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayEarningsJobTitle = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                        <xsl:apply-templates select="jobTitle"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="earnEarnings"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-length">
                <fo:block>
                    <xsl:apply-templates select="earnShift"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="earnHours"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="earnRate"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="earnAmt"/>
                </fo:block>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="earnYtd"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
        </fo:table-row>
    </xsl:template>

    <xsl:template match="deductions">
        <fo:block xsl:use-attribute-sets="section">
            <fo:block xsl:use-attribute-sets="table-caption">
                <xsl:apply-templates select="exsl:node-set($labels)/benefitsTitle"/>
            </fo:block>
            <xsl:choose>
                <xsl:when test="noDeductionData = 'true'">
                    <fo:block xsl:use-attribute-sets="no-data-msg">
                        <xsl:apply-templates select="exsl:node-set($labels)/noBenefitsMessage"/>
                    </fo:block>
                </xsl:when>
                <xsl:otherwise>
                    <fo:table xsl:use-attribute-sets="table">
                        <fo:table-column xsl:use-attribute-sets="deductions-description-column"/>
                        <fo:table-column xsl:use-attribute-sets="deductions-employee-amount-column"/>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-column xsl:use-attribute-sets="deductions-employee-amount-ytd-column"/>
                        </xsl:if>
                        <xsl:if test="exsl:node-set($config)/displayEmployerAmount = 'true'">
                            <fo:table-column xsl:use-attribute-sets="deductions-employer-amount-column"/>
                            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                <fo:table-column xsl:use-attribute-sets="deductions-employer-amount-ytd-column"/>
                            </xsl:if>
                        </xsl:if>
                        <fo:table-column xsl:use-attribute-sets="deductions-applicable-gross-column"/>
                        <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                            <fo:table-column xsl:use-attribute-sets="deductions-applicable-gross-ytd-column"/>
                        </xsl:if>

                        <!-- Print deduction headers -->
                        <fo:table-header>
                            <fo:table-row>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/benefitsDescription"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/benefitsEmployeeAmount"/>
                                    </fo:block>
                                </fo:table-cell>
                                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                        <fo:block>
                                            <xsl:apply-templates select="exsl:node-set($labels)/benefitsYTDEmployeeAmount"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                                <xsl:if test="exsl:node-set($config)/displayEmployerAmount = 'true'">
                                    <fo:table-cell
                                            xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                        <fo:block>
                                            <xsl:apply-templates select="exsl:node-set($labels)/benefitsEmployerAmount"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                        <fo:table-cell
                                                xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                            <fo:block>
                                                <xsl:apply-templates
                                                        select="exsl:node-set($labels)/benefitsYTDEmployerAmount"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </xsl:if>
                                </xsl:if>
                                <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/benefitsApplicableGross"/>
                                    </fo:block>
                                </fo:table-cell>
                                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-column-header table-cell.fixed-decimal">
                                        <fo:block>
                                            <xsl:apply-templates select="exsl:node-set($labels)/benefitsYTDApplicableGross"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                            </fo:table-row>
                        </fo:table-header>
                        <fo:table-body>
                            <xsl:apply-templates select="deductionSections"/>

                            <!-- Print deduction totals -->
                            <fo:table-row>
                                <fo:table-cell xsl:use-attribute-sets="deductions-total-label">
                                    <fo:block>
                                        <xsl:apply-templates select="exsl:node-set($labels)/benefitsTotal"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.total table-cell.fixed-decimal">
                                    <fo:block>
                                        <xsl:apply-templates select="totalEmployeeAmount"/>
                                    </fo:block>
                                </fo:table-cell>
                                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-data table-cell.total table-cell.fixed-decimal">
                                        <fo:block>
                                            <xsl:apply-templates select="totalYTDEmployeeAmount"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                                <xsl:if test="exsl:node-set($config)/displayEmployerAmount = 'true'">
                                    <fo:table-cell
                                            xsl:use-attribute-sets="table-data table-cell.total table-cell.fixed-decimal">
                                        <fo:block>
                                            <xsl:apply-templates select="totalEmployerAmount"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                        <fo:table-cell
                                                xsl:use-attribute-sets="table-data table-cell.total table-cell.fixed-decimal">
                                            <fo:block>
                                                <xsl:apply-templates select="totalYTDEmployerAmount"/>
                                            </fo:block>
                                        </fo:table-cell>
                                    </xsl:if>
                                </xsl:if>
                                <fo:table-cell xsl:use-attribute-sets="table-data">
                                    <fo:block></fo:block>
                                </fo:table-cell>
                                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                                    <fo:table-cell xsl:use-attribute-sets="table-data">
                                        <fo:block></fo:block>
                                    </fo:table-cell>
                                </xsl:if>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                </xsl:otherwise>
            </xsl:choose>
        </fo:block>
    </xsl:template>

    <xsl:template match="deductionSections">
        <fo:table-row>
            <fo:table-cell xsl:use-attribute-sets="deductions-section-title">
                <fo:block>
                    <xsl:apply-templates select="sectionTitle"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block/>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayEmployerAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                    </fo:block>
                </fo:table-cell>
                <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                    <fo:table-cell xsl:use-attribute-sets="table-data">
                        <fo:block>
                        </fo:block>
                    </fo:table-cell>
                </xsl:if>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data">
                    <fo:block>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
        </fo:table-row>

        <xsl:apply-templates select="details" mode="deductions"/>
    </xsl:template>

    <xsl:template match="details" mode="deductions">
        <fo:table-row>
            <fo:table-cell xsl:use-attribute-sets="table-data">
                <fo:block>
                    <xsl:apply-templates select="benefitDeductionDescription"/>
                </fo:block>
            </fo:table-cell>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="employeeAmount"/>
                </fo:block>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="ytdEmployeeAmount"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayEmployerAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="employerAmount"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <xsl:if test="exsl:node-set($config)/displayEmployerAmount = 'true' and exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="ytdEmployerAmount"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
            <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                <fo:block>
                    <xsl:apply-templates select="applicableGrossAmount"/>
                </fo:block>
            </fo:table-cell>
            <xsl:if test="exsl:node-set($config)/displayYTDAmount = 'true'">
                <fo:table-cell xsl:use-attribute-sets="table-data table-cell.fixed-decimal">
                    <fo:block>
                        <xsl:apply-templates select="ytdApplicableGrossAmount"/>
                    </fo:block>
                </fo:table-cell>
            </xsl:if>
        </fo:table-row>
    </xsl:template>

</xsl:stylesheet>